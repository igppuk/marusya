#ifndef MCALL_H
#define MCALL_H

#include <pjsua2.hpp>

using namespace pj;

class QAccount;

class MCall : public Call
{
public:
    MCall(QAccount * acc, int call_id = PJSUA_INVALID_ID);
    ~MCall();

    virtual void onCallState(OnCallStateParam & prm);
    virtual void onCallMediaState(OnCallMediaStateParam & prm);

private:
    QAccount * m_pAcc;
};

#endif // MCALL_H
