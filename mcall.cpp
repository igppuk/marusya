#include "mcall.h"
#include "sipaccount.h"

MCall::MCall(QAccount * acc, int call_id) : Call(*acc->m_pAccount, call_id), m_pAcc(acc)
{
}

MCall::~MCall()
{
}

void MCall::onCallState(OnCallStateParam &prm)
{
    qInfo("onCallState");

    CallInfo ci = getInfo();
    if (ci.state == PJSIP_INV_STATE_DISCONNECTED) {
        m_pAcc->disconnected();
    }
    else if (ci.state == PJSIP_INV_STATE_CALLING) {
        m_pAcc->calling();
    }
    else if (ci.state == PJSIP_INV_STATE_CONFIRMED) {
        // opponent accepted the call
        // show of incoming call dialog
        m_pAcc->confirmed();
    }
}

void MCall::onCallMediaState(OnCallMediaStateParam &prm)
{
    qInfo("onCallMediaState");

    CallInfo ci = getInfo();
    // Iterate all the call medias
    for (unsigned i = 0; i < ci.media.size(); i++) {
       if (ci.media[i].type==PJMEDIA_TYPE_AUDIO && getMedia(i)) {
           AudioMedia *aud_med = (AudioMedia *)getMedia(i);

           // Connect the call audio media to sound device
           AudDevManager& mgr = Endpoint::instance().audDevManager();
           aud_med->startTransmit(mgr.getPlaybackDevMedia());
           mgr.getCaptureDevMedia().startTransmit(*aud_med);
       }
    }
}
