import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.0
import io.qt.marusya.qaccount 1.0

Page {
    id: settingsPage
    width: 600
    height: 400

    title: qsTr("Settings")

    FileDialog {
        id: openImageFileDialog
        modality: Qt.WindowModal
        nameFilters: ["Image File (*.png *.jpg *.bmp)"]

        onAccepted: meImage.source = fileUrl
    }

    Image {
        id: meImage
        width: 100
        height: 100
        fillMode: Image.PreserveAspectCrop
        anchors.left: parent.left
        anchors.leftMargin: 31
        anchors.top: parent.top
        anchors.topMargin: 30
        source: appWindow.sipManager.me.imageUrl === "" ? "qrc:/i/default_avatar1.png" : appWindow.sipManager.me.imageUrl

        MouseArea {
            anchors.fill: parent
            onClicked: {
                openImageFileDialog.open()
            }
        }
    }

    Column {
        id: column
        anchors.right: parent.right
        anchors.rightMargin: 50
        anchors.top: parent.top
        anchors.topMargin: 30
        anchors.left: parent.left
        anchors.leftMargin: 160
        height: 187
        spacing: 5

        Label {
            id: nameLabel
            width: 96
            height: 20
            text: qsTr("Name")
            anchors.left: parent.left
            anchors.leftMargin: 0
            font.bold: true
            font.pointSize: 12
        }

        TextField {
            id: nameTextField
            height: 27
            text: appWindow.sipManager.me.name
            anchors.right: parent.right
            anchors.rightMargin: 13
            anchors.left: parent.left
            anchors.leftMargin: 0
            font.pointSize: 12
            placeholderText: "Enter your name..."
        }

        Label {
            id: phoneNameLabel
            width: 96
            height: 20
            text: qsTr("Phone")
            font.pointSize: 12
            anchors.left: parent.left
            anchors.leftMargin: 0
            font.bold: true
        }

        TextField {
            id: phoneTextField
            height: 27
            text: appWindow.sipManager.me.phone
            font.pointSize: 12
            anchors.left: parent.left
            anchors.leftMargin: 0
            placeholderText: "Phone..."
            anchors.rightMargin: 13
            anchors.right: parent.right
        }

        Label {
            id: passwordNameLabel
            width: 96
            height: 20
            text: qsTr("Password")
            font.pointSize: 12
            anchors.left: parent.left
            anchors.leftMargin: 0
            font.bold: true
        }

        TextField {
            id: passwordTextField
            height: 27
            text: appWindow.sipManager.me.password
            font.pointSize: 12
            anchors.left: parent.left
            anchors.leftMargin: 0
            placeholderText: "Phone..."
            anchors.rightMargin: 13
            anchors.right: parent.right
        }

        Label {
            id: serverNameLabel
            width: 96
            height: 20
            text: qsTr("Server")
            font.pointSize: 12
            anchors.left: parent.left
            anchors.leftMargin: 0
            font.bold: true
        }

        TextField {
            id: serverTextField
            height: 27
            text: appWindow.sipManager.serverUrl
            font.pointSize: 12
            anchors.left: parent.left
            anchors.leftMargin: 0
            placeholderText: "Server..."
            anchors.rightMargin: 13
            anchors.right: parent.right
        }
    }

    Button {
        id: saveButton
        x: 445
        y: 352
        width: 112
        height: 32
        text: qsTr("Save")
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10

        onClicked: {
            appWindow.sipManager.me.name = nameTextField.text
            appWindow.sipManager.me.phone = phoneTextField.text
            appWindow.sipManager.me.password = passwordTextField.text
            appWindow.sipManager.me.imageUrl = meImage.source
            appWindow.sipManager.serverUrl = serverTextField.text
            appWindow.sipManager.SaveAccount(appWindow.sipManager.me)
        }
    }
}
