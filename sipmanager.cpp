#include "sipmanager.h"
#include <pjsua2.hpp>

SipManager::SipManager(QObject *parent) : QObject(parent)
{
    m_pEndPoint = new Endpoint();
    m_pEndPoint->libCreate();

    // Initialize endpoint
    EpConfig ep_cfg;

    //ep_cfg.logConfig.level = 5;
    //ep_cfg.logConfig.filename = "d:/my_projects/voip/build-marusya-Desktop_Qt_5_10_1_MSVC2017_64bit-Debug/debug/sip_logger.txt";
    //ep_cfg.uaConfig.maxCalls = 4;

    m_pEndPoint->libInit(ep_cfg);

    // Create SIP transport. Error handling sample is shown
    TransportConfig tcfg;
    tcfg.port = 5060;
    try
    {
        m_pEndPoint->transportCreate(PJSIP_TRANSPORT_UDP, tcfg);

        // Start the library (worker threads etc)
        m_pEndPoint->libStart();
        qInfo("*** PJSUA2 STARTED ***");
    }
    catch (Error &err)
    {
        qFatal(err.info().c_str());
    }

    // create me
    m_pMe = new QAccount();

    m_serverUrl = "192.168.56.101";
    m_serverPort = "5060";
}

SipManager::~SipManager()
{
    m_pMe->deleteLater();
    m_pEndPoint->libDestroy();
    m_player->deleteLater();

    delete m_pEndPoint;
}

QAccount * SipManager::me()
{
    return m_pMe;
}

QAccount * SipManager::createAccount()
{
    auto account = new QAccount();
    m_accounts.append(account);

    return account;
}

void SipManager::destroyAccount(QAccount * account)
{
    m_accounts.removeOne(account);
    delete account;
    account = 0;
}

QString SipManager::serverUrl()
{
    return m_serverUrl;
}

void SipManager::setServerUrl(const QString & url)
{
    m_serverUrl = url;
}

QString SipManager::serverPort()
{
    return m_serverPort;
}

void SipManager::setServerPort(const QString & port)
{
    m_serverPort = port;
}

int SipManager::volume()
{
    return m_player->volume();
}

void SipManager::setVolume(int volume)
{
    m_player->setVolume(volume);
}

void SipManager::saveAccount(QAccount * account)
{
}

void SipManager::connectToServer(const QString & url)
{
    me()->create(url);
    //pj_thread_sleep(10000);
}

void SipManager::initPlayer()
{
    m_player = new QMediaPlayer();
    auto url = QUrl("qrc:/s/ring1.mp3");
    auto mediaContent = QMediaContent(url);
    m_player->setMedia(mediaContent);
    m_player->setVolume(100);
}

void SipManager::playSound()
{
    m_player->play();
}

void SipManager::stopSound()
{
    m_player->stop();
}
