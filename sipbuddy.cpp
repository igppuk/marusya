#include "sipbuddy.h"

SipBuddy::SipBuddy(QBuddy * pQBuddy)
{
    m_pQBuddy = pQBuddy;
}

SipBuddy::~SipBuddy()
{
}

void SipBuddy::onBuddyState()
{
    qInfo("onBuddyState");

    BuddyInfo bi = getInfo();
    //m_pQBuddy->onBuddyState(bi);
}

void SipBuddy::onBuddyEvSubState(OnBuddyEvSubStateParam &prm)
{
    qInfo("onBuddyEvSubState");
}

QBuddy::QBuddy(QObject *parent): QObject(parent)
{
    qInfo("new buddy created");
    m_pBuddy = new SipBuddy(this);
}

QBuddy::~QBuddy()
{
    qInfo("buddy destroyed");
    delete m_pBuddy;
}

void QBuddy::create(const QString & url, Account & acc)
{
    BuddyConfig cfg;
    cfg.uri = url.toStdString(); // "sip:alice@example.com";
    cfg.subscribe = true;
    try {
        m_pBuddy->create(acc, cfg);
        m_pBuddy->subscribePresence(true);
    } catch(Error& err) {
        qInfo(err.info().c_str());
    }
}

void QBuddy::onBuddyState(BuddyInfo & bi)
{
}
