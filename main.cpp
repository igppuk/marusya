#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <pjsua2.hpp>
#include "sipmanager.h"

using namespace pj;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<SipManager>("io.qt.marusya.sipmanager", 1, 0, "SipManager");
    qmlRegisterType<QAccount>("io.qt.marusya.qaccount", 1, 0, "QAccount");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
