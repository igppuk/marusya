#ifndef SIPACCOUNT_H
#define SIPACCOUNT_H

#include <QObject>
#include <QSharedPointer>
#include <pjsua2.hpp>

#include "mcall.h"
#include "sipbuddy.h"

using namespace pj;

enum Status
{
    Online = 0,
    Away = 1,
    Busy = 2,
    Offline = 3
};

class SipAccount;
class QAccount : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString password READ password WRITE setPassword)
    Q_PROPERTY(QString phone READ phone WRITE setPhone)
    Q_PROPERTY(QString imageUrl READ imageUrl WRITE setImageUrl NOTIFY imageUrlChanged)
<<<<<<< HEAD
    Q_PROPERTY(bool connected READ connected)
    Q_PROPERTY(int status READ status)
    Q_OBJECT
=======
    Q_PROPERTY(QList<QBuddy *> * buddies READ buddies)
>>>>>>> 93ad47ad0592ad9bef83f2e7a0c11a303bac5579

public:
    explicit QAccount(QObject *parent = nullptr);
    ~QAccount();

    QString name();
    void setName(const QString & name);
    QString password();
    void setPassword(const QString & password);
    QString phone();
    void setPhone(const QString & phone);
    QString imageUrl();
    void setImageUrl(const QString & imageUrl);
    bool connected();
    int status();

    void OnRegState(OnRegStateParam &prm);
    void OnIncomingCall(const OnIncomingCallParam & iprm);

    Q_INVOKABLE void answer();
    Q_INVOKABLE void handup();

    Q_INVOKABLE void call(const QString & url);
    Q_INVOKABLE void setStatus(int status);

    QList<QBuddy *> * buddies();
    Q_INVOKABLE QBuddy * AddBuddy(const QString & url);

    void create(const QString & url);

    AccountConfig m_acfg;
    SipAccount * m_pAccount;

signals:
    void disconnected();
    void serverFailed();
    void serverConnected();
    void incomingCall();
    void calling();
    void confirmed();
    void imageUrlChanged();

private:
    QString m_name;
    QString m_password;
    QString m_phone;
    QString m_imageUrl;
    MCall * m_pCall;
<<<<<<< HEAD
    bool m_bServerConnected;
    int m_iStatus;
=======
    QList<QBuddy *> m_buddies;
>>>>>>> 93ad47ad0592ad9bef83f2e7a0c11a303bac5579

    void removeCall();
};

class SipAccount : public Account
{
public:
    SipAccount(QAccount * pQAccount);
    ~SipAccount();

    virtual void onRegState(OnRegStateParam &prm);
    virtual void onIncomingCall(OnIncomingCallParam &iprm);

    virtual void onRegStarted(OnRegStartedParam &prm);
    virtual void onIncomingSubscribe(OnIncomingSubscribeParam &prm);
    virtual void onInstantMessage(OnInstantMessageParam &prm);
    virtual void onInstantMessageStatus(OnInstantMessageStatusParam &prm);
    virtual void onTypingIndication(OnTypingIndicationParam &prm);
    virtual void onMwiInfo(OnMwiInfoParam &prm);

private:
    QAccount * m_pQAccount;
};

#endif // SIPACCOUNT_H
