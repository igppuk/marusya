import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

Window {
    id: incomingCall
    width: 476
    height: 400
    color: "#2aac8f"

    Component.onCompleted: {
        console.log("incoming component loaded")
        var me = window.sipManager.me
        window.sipManager.playSound()
    }

    //    window: {
    //        flags = Qt.Popup | Qt.FramelessWindowHint | Qt.CustomizeWindowHint
    //    }

    onVisibleChanged: {
        if (!visible) {
            window.sipManager.me.handup()
            window.sipManager.stopSound()
        }
    }

    Column {
        id: column
        height: parent.height
        width: parent.width
        spacing: 15

        Label {
            id: nameLabel
            height: 30

            color: "white"
            text: qsTr("Incomin name...")
            horizontalAlignment: Text.AlignHCenter
            anchors.left: parent.left
            anchors.right: parent.right

            font.pixelSize: 24
        }

        Image {
            id: incomingImage
            height: 250

            anchors.top: nameLabel.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.right: parent.right
        }

        Row {
            id: imagesRow
            anchors.top: incomingImage.bottom
            anchors.topMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 30
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 20

            Image {
                id: handUpImage
                width: 64
                height: 64

                anchors.left: parent.left
                anchors.leftMargin: 10
                fillMode: Image.PreserveAspectFit

                source: "qrc:/i/handup.png"

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        handUpBorder.visible = true
                    }

                    onExited: {
                        handUpBorder.visible = false
                    }

                    onClicked: {
                        console.log("hand up")
                        window.sipManager.me.handup()
                        window.sipManager.stopSound()
                    }
                }

                Rectangle {
                    id: handUpBorder
                    anchors.fill: parent
                    visible: false

                    border.width: 2
                    border.color: "black"
                    color: "transparent"
                }
            }

            Image {
                id: pickupImage
                width: 64
                height: 64

                anchors.right: parent.right
                anchors.rightMargin: 10
                fillMode: Image.PreserveAspectFit

                source: "qrc:/i/pickup.png"

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        pickUpBorder.visible = true
                    }

                    onExited: {
                        pickUpBorder.visible = false
                    }

                    onClicked: {
                        console.log("pick up")
                        window.sipManager.me.answer()
                        window.sipManager.stopSound()

                        pickupImage.visible = false
                    }
                }

                Rectangle {
                    id: pickUpBorder
                    anchors.fill: parent
                    visible: false

                    border.width: 2
                    border.color: "black"
                    color: "transparent"
                }
            }

            Image {
                id: videoPickupImage
                width: 64
                height: 64

                anchors.right: pickupImage.left
                anchors.rightMargin: 40
                fillMode: Image.PreserveAspectFit

                source: "qrc:/i/video_pickup.png"

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        videoPickUpBorder.visible = true
                    }

                    onExited: {
                        videoPickUpBorder.visible = false
                    }

                    onClicked: {
                        console.log("video")
                        window.sipManager.stopSound()
                    }
                }

                Rectangle {
                    id: videoPickUpBorder
                    anchors.fill: parent
                    visible: false

                    border.width: 2
                    border.color: "black"
                    color: "transparent"
                }
            }
        }

        Slider {
            id: volumeSlider
            anchors.top: imagesRow.bottom
            anchors.topMargin: 10
            anchors.left: parent.left
            anchors.leftMargin: 20
            anchors.right: parent.right
            anchors.rightMargin: 20

            minimumValue: 0
            maximumValue: 100
            value: window.sipManager.volume

            onValueChanged: {
                window.sipManager.volume = value
            }
        }
    }

    // Methods

    function show() {
        incomingCall.visible = true
    }

    function hide() {
        incomingCall.visible = false
    }
}
