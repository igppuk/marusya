import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.2
import io.qt.marusya.sipmanager 1.0
import io.qt.marusya.qaccount 1.0

ApplicationWindow {
    id: window
    property ApplicationWindow appWindow : window
    property var incomingCallWindow

    visible: true
    width: 640
    height: 550
    title: qsTr("Stack")

    property var sipManager: SipManager {
        id: sm

        me.onIncomingCall: {
            console.log("incoming call")
            incomingCallWindow = null

            var component = Qt.createComponent("IncomingCall.qml");
            if (component.status === Component.Ready) {
                incomingCallWindow = component.createObject(window);
                incomingCallWindow.show()
            }
            else {
                var e = component.errorString()
                showError(e)
            }
        }

        me.onServerConnected: {
            console.log("server connected")
            //var connectionButton = stackView.find(function(item, index) { return item.objectName === "connectionButton" })
            loader1.item.setConnectionStratus(qsTr("Connected..."), false)
            loader1.item.setStatusDropdown(0, true)

            // set online status
            me.setStatus(0)
            initPlayer();
        }

        me.onServerFailed: {
            console.log("server failed")
            loader1.item.setConnectionStratus(qsTr("Failed..."), true)
        }

        me.onDisconnected: {
            console.log("call disconnected")

            if (incomingCallWindow !== null) {
                incomingCallWindow.visible = false
                incomingCallWindow = null
            }
        }

        me.onCalling: {
            console.log("calling")
            incomingCallWindow = null

            var component = Qt.createComponent("IncomingCall.qml");
            if (component.status === Component.Ready) {
                incomingCallWindow = component.createObject(window);
                incomingCallWindow.show()
            }
            else {
                var e = component.errorString()
                showError(e)
            }
        }

        me.onConfirmed: {
            console.log("call confirmed")
            stopSound()
        }
    }

    onClosing: {
        // set offline status
        sipManager.me.setStatus(3)
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Page 1")
                width: parent.width
                onClicked: {
                    stackView.push("Page1Form.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push("Settings.ui.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: homePage
        anchors.fill: parent
    }

    Page {
        id: homePage
        x: 0
        width: 600
        height: 450
        title: qsTr("Main Window")

        TabBar {
            id: tabBar
            width: homePage.width

            onCurrentIndexChanged: {
                switch (currentIndex)
                {
                case 0:
                    loader2.visible = false
                    loader1.visible = true
                    //loader1.source = "qrc:/Call.qml"
                    break;
                case 1:
                    loader1.visible = false
                    loader2.visible = true
                    //loader2.source = "qrc:/Buddy.qml"
                    break;
                }

            }

            TabButton {
                x: 113
                y: 12
                width: 80
                Text {
                    text: qsTr("Call")
                    font.family: "Times New Roman"
                    font.pointSize: 18
                    color: "white"
                }
            }

            TabButton {
                width: 80
                Text {
                    text: qsTr("Buddies")
                    font.family: "Times New Roman"
                    font.pointSize: 18
                    color: "white"
                }
            }
        }

        Loader {
            id: loader1
            anchors.fill: parent
            anchors.top:  tabBar.bottom
            anchors.topMargin: 35
            source: "qrc:/Call.qml"

            onLoaded: {
<<<<<<< HEAD
=======
                console.log("call loaded")
                if (item) {
                    item.appWindow = appWindow
                }
            }
        }

        Loader {
            id: loader2
            anchors.fill: parent
            anchors.top:  tabBar.bottom
            anchors.topMargin: 35
            source: "qrc:/Buddy.qml"

            onLoaded: {
                console.log("buddy loaded")
>>>>>>> 93ad47ad0592ad9bef83f2e7a0c11a303bac5579
                if (item) {
                    item.appWindow = appWindow
                    item.resfesh(sipManager)
                }
            }
        }
    }

    MessageDialog {
        id: messageDialog
        title: "Message"
        text: ""
        visible: false

        onAccepted: {
            visible = false
        }
    }

    // Methods

    function showError(message) {
        messageDialog.text = message
        messageDialog.visible = true
    }
}
