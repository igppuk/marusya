#ifndef SIPMANAGER_H
#define SIPMANAGER_H

#include <QObject>
#include <QList>
#include <QSharedPointer>
#include <QMediaPlayer>

#include <QSharedPointer>
#include "sipaccount.h"

#include <pjsua2.hpp>

using namespace pj;

class SipManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString serverUrl READ serverUrl WRITE setServerUrl NOTIFY serverUrlChanged)
    Q_PROPERTY(QString serverPort READ serverPort WRITE setServerPort NOTIFY serverPortChanged)
    Q_PROPERTY(int volume READ volume WRITE setVolume NOTIFY volumeChanged)
    Q_PROPERTY(QAccount * me READ me)

public:
    explicit SipManager(QObject *parent = nullptr);
    ~SipManager();

    Q_INVOKABLE QAccount * createAccount();
    Q_INVOKABLE void destroyAccount(QAccount * account);
    Q_INVOKABLE void saveAccount(QAccount * account);

    Q_INVOKABLE void connectToServer(const QString & url);

    Q_INVOKABLE void initPlayer();
    Q_INVOKABLE void playSound();
    Q_INVOKABLE void stopSound();

    QAccount * me();

    QString serverUrl();
    void setServerUrl(const QString & url);

    QString serverPort();
    void setServerPort(const QString & port);

    int volume();
    void setVolume(int volume);

signals:
    void serverUrlChanged();
    void serverPortChanged();
    void volumeChanged();

public slots:

private:
    QString m_serverUrl;
    QString m_serverPort;
    QAccount * m_pMe;
    QList<QAccount *> m_accounts;
    Endpoint * m_pEndPoint;
    QMediaPlayer * m_player;
};

#endif // SIPMANAGER_H
