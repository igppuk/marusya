import QtQuick 2.0
import QtQuick.Controls 2.3

Item {
    id: callItem
    property ApplicationWindow appWindow

    Row {
        id: row
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.leftMargin: 10
        height: 100
        anchors.rightMargin: 10
        spacing: 20

        Image {
            id: meImage
            width: 100
            height: 100
            fillMode: Image.PreserveAspectCrop
            source: appWindow.sipManager.me.imageUrl
                    === "" ? "qrc:/i/default_avatar1.png" : appWindow.sipManager.me.imageUrl
        }

        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            spacing: 10
            anchors.leftMargin: 120
            anchors.rightMargin: 10

            Label {
                id: nameLabel
                width: 96
                height: 20
                text: qsTr("Name: ") + ((appWindow.sipManager.me.name
                                         === "") ? "No name" : appWindow.sipManager.me.name)
                font.bold: true
                font.pointSize: 12
            }

            Label {
                id: phoneLabel
                width: 96
                height: 20
                text: qsTr("Phone: ") + ((appWindow.sipManager.me.phone
                                          === "") ? "No phone" : appWindow.sipManager.me.phone)
                font.bold: true
                font.pointSize: 12
            }

            Label {
                id: serverLabel
                width: 96
                height: 20
                text: qsTr("Server: ") + ((appWindow.sipManager.serverUrl
                                           === "") ? "No server" : appWindow.sipManager.serverUrl)
                font.bold: true
                font.pointSize: 12
            }
        }
    }

    Row {
        id: rowStatus
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.top: row.bottom
        anchors.topMargin: 10
        height: 40
        spacing: 20

        Button {
            id: connectionButton
            objectName: "connectionButton"

            text: qsTr("Connect to server...")

            onClicked: {
                connectionButton.text = qsTr("Wait...")
                connectionButton.enabled = false
                var url = appWindow.sipManager.serverUrl + ":" + appWindow.sipManager.serverPort
                appWindow.sipManager.connectToServer(url)
            }
        }

        ComboBox {
            id: statusCombobox
            textRole: "key"
            currentIndex: 3
            enabled: false

            model: ListModel {
                id: modelStatuses
                ListElement { key: qsTr("Online"); color: "Green" }
                ListElement { key: qsTr("Away"); color: "Yellow" }
                ListElement { key: qsTr("Busy"); color: "Brown" }
                ListElement { key: qsTr("Offline"); color: "Red" }
            }

            onActivated: {
                console.log("activated")
                sipManager.me.setStatus(index)
            }
        }
    }

    Row {
        id: rowCall
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.top: rowStatus.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: 10
        spacing: 20

        Column {
            id: phoneColumn
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            width: 250

            Row {
                id: cellRow
                anchors.left: parent.left
                anchors.top: parent.top
                height: 33
                anchors.right: parent.right
                spacing: 10

                TextField {
                    id: cellNumberTextField
                    height: 27
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.rightMargin: 70
                    font.pointSize: 12
                    placeholderText: qsTr("Enter phone number...")

                    validator: RegExpValidator {
                        regExp: /[0-9\*\#]+/
                    }

                    onTextChanged: {
                        callButton.enabled = (cellNumberTextField.text !== "")
                    }
                }

                Button {
                    id: callButton
                    anchors.top: parent.top
                    anchors.left: cellNumberTextField.right
                    anchors.leftMargin: 5
                    height: 27
                    enabled: false

                    text: qsTr("Call")

                    onClicked: {
                        console.log("making call..." + cellNumberTextField.text)
                        var url = "sip:" + cellNumberTextField.text + "@" + sipManager.serverUrl + ":" + sipManager.serverPort
                        sipManager.me.call(url)
                    }
                }
            }

            Grid {
                anchors.top: cellRow.bottom

                columns: 3
                rows: 5
                spacing: 2

                Button {
                    width: 40
                    height: 40

                    text: qsTr("1")
                    onClicked: {
                        appendCellNumber("1")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("2")
                    onClicked: {
                        appendCellNumber("2")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("3")
                    onClicked: {
                        appendCellNumber("3")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("4")
                    onClicked: {
                        appendCellNumber("4")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("5")
                    onClicked: {
                        appendCellNumber("5")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("6")
                    onClicked: {
                        appendCellNumber("6")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("7")
                    onClicked: {
                        appendCellNumber("7")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("8")
                    onClicked: {
                        appendCellNumber("8")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("9")
                    onClicked: {
                        appendCellNumber("9")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("*")
                    onClicked: {
                        appendCellNumber("*")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("0")
                    onClicked: {
                        appendCellNumber("0")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("#")
                    onClicked: {
                        appendCellNumber("#")
                    }
                }

                Button {
                    width: 40
                    height: 40

                    text: qsTr("<<")
                    onClicked: {
                        appendCellNumber("del")
                    }
                }
            }
        }
    }

    // Methods

    function resfesh(sipManager) {
        // restore connection status
        if (sipManager.me.connected) {
            connectionButton.text = qsTr("Connected...")
            connectionButton.enabled = false
        }

        // restore status dropdown
        if (sipManager.me.status !== 3) {
            setStatusDropdown(sipManager.me.status, true)
        }
    }

    function setConnectionStratus(text, enabled) {
        connectionButton.text = qsTr("Connected...")
        statusCombobox.enabled = enabled
    }

    function setStatusDropdown(index, enabled) {
        statusCombobox.enabled = enabled
        statusCombobox.currentIndex = index
    }

    function appendCellNumber(number) {
        if (number !== "del") {
            if (cellNumberTextField.text === "") {
                cellNumberTextField.text = number
            }
            else {
                cellNumberTextField.text += number
            }
        }
        else {
            if (cellNumberTextField.text !== "") {
                cellNumberTextField.text = cellNumberTextField.text.slice(0,-1)
            }
        }

        callButton.enabled = (cellNumberTextField.text !== "")
    }
}
