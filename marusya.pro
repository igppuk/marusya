QT += core quick multimedia

CONFIG += c++11 thread

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES -= UNICODE

QMAKE_LFLAGS_DEBUG += /INCREMENTAL:NO

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

RESOURCES += d:/my_projects/voip/marusya/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

SOURCES += d:/my_projects/voip/marusya/main.cpp \
    d:/my_projects/voip/marusya/sipaccount.cpp \
    d:/my_projects/voip/marusya/sipmanager.cpp \
    d:/my_projects/voip/marusya/mcall.cpp \
    d:/my_projects/voip/marusya/sipbuddy.cpp

LIBS += -L$$_PRO_FILE_PWD_/lib/debug/x64/ -lbaseclasses \
    -lg7221codec \
    -lgsmcodec \
    -lccodec \
    -lmilenage \
    -lpjproject \
    -lresample \
    -lspeex \
    -lsrtp \
    -lwebrtc \
    -lyuv \
    -lpjlib\
    -lpjlib-util \
    -lpjmedia \
    -lpjmedia-audiodev \
    -lpjmedia-codec \
    -lpjmedia-videodev \
    -lpjnath \
    -lpjsip-core \
    -lpjsip-simple \
    -lpjsip-ua \
    -lpjsua \
    -lpjsua2 \
    -lSDL2 \
    -lSDL2main \
    -lws2_32 \
    -lwsock32 \
    -lole32

INCLUDEPATH += d:/my_projects/voip/marusya/include2
DEPENDPATH += d:/my_projects/voip/marusya/include2

HEADERS += d:/my_projects/voip/marusya/sipaccount.h \
    d:/my_projects/voip/marusya/sipmanager.h \
    d:/my_projects/voip/marusya/mcall.h \
    d:/my_projects/voip/marusya/sipbuddy.h
