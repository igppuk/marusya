#include "sipaccount.h"

SipAccount::SipAccount(QAccount * pQAccount)
{
    m_pQAccount = pQAccount;
}

SipAccount::~SipAccount()
{
}

void SipAccount::onRegState(OnRegStateParam & prm)
{
    qInfo("onRegState");
    AccountInfo ai = getInfo();
    m_pQAccount->OnRegState(prm);
    //qInfo(ai.regIsActive ? "*** Register" : "*** Unregister);
}

void SipAccount::onIncomingCall(OnIncomingCallParam &iprm)
{
    qInfo("onIncomingCall");
    m_pQAccount->OnIncomingCall(iprm);
}

void SipAccount::onRegStarted(OnRegStartedParam &prm)
{
    qInfo("onRegStarted");
}

void SipAccount::onIncomingSubscribe(OnIncomingSubscribeParam &prm)
{
    qInfo("onRegStarted");
}

void SipAccount::onInstantMessage(OnInstantMessageParam &prm)
{
    qInfo("onInstantMessage");
}

void SipAccount::onInstantMessageStatus(OnInstantMessageStatusParam &prm)
{
    qInfo("onInstantMessageStatus");
}

void SipAccount::onTypingIndication(OnTypingIndicationParam &prm)
{
    qInfo("onTypingIndication");
}

void SipAccount::onMwiInfo(OnMwiInfoParam &prm)
{
    qInfo("onMwiInfo");
}

QAccount::QAccount(QObject *parent) : QObject(parent), m_pCall(0)
{
    qInfo("new sip account created");

    m_pAccount = new SipAccount(this);
    m_name = "vladimir lozhnikov";
    m_password = "1";
    m_phone = "102";
    m_bServerConnected = false;
    m_iStatus = 3;
}

QAccount::~QAccount()
{
    delete m_pCall;
    delete m_pAccount;
    qInfo(QString("sip account destroyed: %1").arg(m_name).toStdString().c_str());
}

QString QAccount::name()
{
    return m_name;
}

void QAccount::setName(const QString &name)
{
    m_name = name;
}

QString QAccount::password()
{
    return m_password;
}

void QAccount::setPassword(const QString & password)
{
    m_password = password;
}

QString QAccount::phone()
{
    return m_phone;
}

void QAccount::setPhone(const QString & phone)
{
    m_phone = phone;
}

QString QAccount::imageUrl()
{
    return m_imageUrl;
}

void QAccount::setImageUrl(const QString & imageUrl)
{
    m_imageUrl = imageUrl;
}

bool QAccount::connected()
{
    return m_bServerConnected;
}

int QAccount::status()
{
    return m_iStatus;
}

void QAccount::OnRegState(OnRegStateParam & prm)
{
    if (prm.code == PJSIP_SC_OK)
    {
        m_bServerConnected = true;
        emit serverConnected();
    }
    else if (prm.code == PJSIP_SC_BAD_REQUEST ||
             prm.code == PJSIP_SC_SERVICE_UNAVAILABLE ||
             prm.code == PJSIP_SC_REQUEST_TIMEOUT)
    {
        m_bServerConnected = false;
        emit serverFailed();
    }
}

void QAccount::OnIncomingCall(const OnIncomingCallParam & iprm)
{
    m_pCall = new MCall(this, iprm.callId);
    emit incomingCall();

    connect(this, &QAccount::disconnected, [=]() {
        removeCall();
    });
}

void QAccount::answer()
{
    Q_ASSERT(m_pCall);

    CallOpParam prm;
    prm.statusCode = PJSIP_SC_OK;
    prm.reason = "marusya answer";
    m_pCall->answer(prm);
}

void QAccount::handup()
{
    if (!m_pCall) {
        return;
    }

    CallOpParam prm;
    prm.reason = "marusya hangup";
    m_pCall->hangup(prm);

    emit disconnected();
}

void QAccount::call(const QString & url)
{
    removeCall();

    m_pCall = new MCall(this);
    CallOpParam prm(true); // Use default call settings
    try {
        m_pCall->makeCall(url.toStdString(), prm);

        connect(this, &QAccount::disconnected, [=]() {
            removeCall();
        });
    } catch(Error& err) {
        qFatal(err.info().data());
    }
}

void QAccount::setStatus(int status)
{
    if (!m_pAccount->isValid())
    {
        return;
    }

    PresenceStatus ps;
    ps.status = PJSUA_BUDDY_STATUS_ONLINE;

    switch (status)
    {
        case Online:
            ps.note = "On the phone";
        break;
        case Away:
            ps.activity = PJRPID_ACTIVITY_AWAY;
            ps.note = "Away";
        break;
        case Busy:
            ps.activity = PJRPID_ACTIVITY_BUSY;
            ps.note = "Do not disturb";
        break;
        case Offline:
            ps.status = PJSUA_BUDDY_STATUS_OFFLINE;
            ps.note = "Offline";
        break;
    };

    m_iStatus = status;
    m_pAccount->setOnlineStatus(ps);
}

QList<QBuddy *> * QAccount::buddies()
{
    return &m_buddies;
}

QBuddy *QAccount::AddBuddy(const QString &url)
{
    QBuddy * pBuddy = new QBuddy();
    pBuddy->create(url, *m_pAccount);

    m_buddies.append(pBuddy);
    return pBuddy;
}

void QAccount::create(const QString &url)
{
    auto idUri = QString("sip:%1@%2").arg(phone()).arg(url);
    m_acfg.idUri = idUri.toStdString();
    m_acfg.regConfig.registrarUri = QString("sip:%1").arg(url).toStdString();
    m_acfg.presConfig.publishEnabled = true;
    AuthCredInfo cred("digest", "*", phone().toStdString(), 0, password().toStdString());
    m_acfg.sipConfig.authCreds.push_back(cred);

    // Create the account
    try {
        m_pAccount->create(m_acfg);
    } catch(Error& err) {
        qInfo(err.info().c_str());
    }

    //pj_thread_sleep(10000);
}

void QAccount::removeCall()
{
    if (m_pCall) {
        delete m_pCall;
        m_pCall = 0;
    }
}
