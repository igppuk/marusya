#ifndef SIPBUDDY_H
#define SIPBUDDY_H

#include <QObject>
#include <pjsua2.hpp>

using namespace pj;

class SipBuddy;
class QBuddy : public QObject
{
    Q_OBJECT

public:
    explicit QBuddy(QObject *parent = nullptr);
    ~QBuddy();

    void create(const QString & url, Account & pAccount);
    void onBuddyState(BuddyInfo & bi);

    BuddyConfig m_cfg;
    SipBuddy * m_pBuddy;

signals:

private:
};

class SipBuddy : public Buddy
{
public:
    SipBuddy(QBuddy * pQBuddy);
    ~SipBuddy();

    virtual void onBuddyState();
    virtual void onBuddyEvSubState(OnBuddyEvSubStateParam & prm);

private:
    QBuddy * m_pQBuddy;
};

#endif // SIPBUDDY_H
